const { app, BrowserWindow, ipcMain, dialog } = require("electron");
const Store = require("electron-store");
const path = require("path");

const store = new Store();

const travels = store.has("travels") ? store.get("travels") : [];

if (travels.length === 0) {
  travels.push({
    id: 1,
    image:
      "https://images.pexels.com/photos/2356087/pexels-photo-2356087.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "Randonnée entre toundra, glaciers et icebergs",
    destination: "Pôle Nord: Groenland",
    description:
      "Cette région du Groenland offre aux randonneurs le choc d’une mer couverte d’icebergs...",
    longDescription:
      "Cette région du Groenland offre aux randonneurs le choc d’une mer couverte d’icebergs et l’impressionnante beauté des fronts glaciaires tombant dans la mer. Le contraste des couleurs entre les glaces et la toundra luxuriante donne du caractère à ce voyage à la portée de tout randonneur. Les transferts en bateau dans les fjords entre les icebergs bleus vous permettent d’accéder à des randonnées à la journée, en faisant une marche sur glacier avec ses grandes fissures et crevasses et en visitant d’anciens sites vikings. Bienvenue dans le dédale impressionnant des fjords les plus profonds et des couleurs bleutées de la glace du Groenland ! Ces tarifs sont valables pour des départs de Paris. Des départs d'autres villes sont possibles, n'hésitez pas à nous en faire la demande. Les prix indiqués ne sont valables que pour certaines classes de réservation sur les compagnies aériennes qui desservent cette destination. Ils sont donc susceptibles d'être modifiés en cas d'indisponibilité de places dans ces classes au moment de la réservation. Le prix comprend Le transport aérien et les transferts, 2 nuits en Islande en guest house, 4 nuits au Groenland en auberge, 3 nuits au Groenland sous tente montée, le guide anglophone ou francophone suivant les dates, la pension complète au Groenland du dîner du J2 au petit déjeuner du J9, une parka chaude et un gilet de sauvetage pour les transfères en bateaux, les crampons et équipement de sécurité. Le prix ne comprend pas: Les frais d'inscription, les assurances, les repas en Islande et au Groenland non mentionnés, les excursions en option et entrées dans les musées.Complément d'information tarifaire: Si vous voyagez seul(e), un supplément single de 180 € sera appliqué sur le prix de votre voyage. A noter qu'il n'y a pas de possibilité de chambre single au Groenland. Ce supplément ne s’applique que pour les nuits en Islande",
    tags: "Fjord, Iceberg, Arctique, Reykjavik, Banquise, Inlandsis",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    avantages: "Circuit accompagné, Randonnée, 10 jours, vol inclus",
    tags: "",
    like: 90,
    duree: 24,
    price: 4999,
  });
  travels.push({
    id: 2,
    image:
      "https://images.unsplash.com/photo-1615376506233-f0821173ddee?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDZ8fHxlbnwwfHx8&auto=format&fit=crop&w=500&q=60",
    title: "Fjords et glaciers en kayak au Spitzberg",
    destination: "Pôle Nord: Spitzberg",
    description:
      "Perchés tout au nord de l'Immense Isfjord, se trouvent les fjords Dickson et Ekman...",
    longDescription:
      "Circuit accompagné, Croisière, 9 jours, vol inclus: Nous vous proposons une découverte très confidentielle des territoires les plus inexplorés de l'Isfjord. Perchés tout au nord de l'Immense Isfjord, se trouvent les fjords Dickson et Ekman, deux fjords cachés aux yeux de tous et qui renferment plusieurs trésors. Tout d'abord, les montagnes du Kapitol et celles du Kolosseum, véritables sculptures minérales orchestrées par l'érosion naturelle. S'ensuit l'imposant glacier Sefstrombreen, avec un front de glace de près de 2,9 km de long. Notre campement sur l'île de Flintholem nous donne une vue extraordinaire sur ces premiers trésors. Nous terminons notre exploration par l'immense glacier de Svéa et ses 3,6 km de large. Un monstre de glace d’où résonne l'étrange grondement de la glace en mouvement. Un voyage extraordinaire, loin, très loin des sentiers battus.",
    avantages: "Circuit accompagné, Croisière, 9 jours, vol inclus",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    tags: "",
    like: 90,
    duree: 24,
    price: 3699,
  });
  travels.push({
    id: 3,
    image:
      "https://images.pexels.com/photos/219837/pexels-photo-219837.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "Cap Horn et détroit de Magellan",
    destination: "Pôle Sud: Chili, Argentine",
    description:
      "Les deux bateaux, le Stella Australis et le Ventus Australis, sont 2 navires similaires de la flotte chilienne. Uniques en leur genre",
    longDescription:
      "Les deux bateaux, le Stella Australis et le Ventus Australis, sont 2 navires similaires de la flotte chilienne. Uniques en leur genre, ils proposent des navigations dans le détroit de Magellan pour aller à la découverte des glaciers de la cordillère Darwin, en Terre de Feu. Après avoir emprunté le canal Beagle, le bateau descend jusqu'au cap Horn, avec un possible débarquement sur l'île Horn. Vous pourrez saluer les gardiens du phare le plus mythique. Ensuite, le bateau remonte vers la ville la plus australe du monde, Ushuaia. Il est possible d'effectuer la navigation dans le sens inverse (les deux navigations proposées sont complémentaires).",
    avantages:
      "• Le mythique cap Horn et le canal Beagle, • La découverte des glaciers de la cordillère Darwin, • Des bateaux de petite capacité, • Des débarquements quotidiens, • Une prestation tout inclus à bord",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    tags: "",
    like: 90,
    duree: 24,
    price: 3999,
  });
  travels.push({
    id: 4,
    image:
      "https://images.unsplash.com/photo-1614597388646-fea96377eb91?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    title: "Découvert du désert de Sibérie",
    destination: "Pôle Nord: Sibérie",
    description:
      "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence...",
    longDescription:
      "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...Nous sommes engagés dans une démarche de tourisme responsable.Nous souhaitons vous faire connaître et vous associer à notre action aux côtés d'Agir pour un Tourisme Responsable (ATR). Pour plus d’informations avant de partir, lisez la charte éthique du voyageur et notre charte GNGL.En route vers une belle planète...Un trajet collectif Et n'oubliez pas qu'un geste pour la planète commence dès le début du voyage: pensez au covoiturage pour vos pré- acheminements! Une façon conviviale de diminuer votre consommation de CO2 et votre budget transport",
    avantages: "Circuit accompagné, Randonnée, 12 jours, vol inclus",
    tags: "",
    like: 90,
    duree: 24,
    price: 5499,
  });
  travels.push({
    id: 5,
    image:
      "https://images.pexels.com/photos/255329/pexels-photo-255329.jpeg?cs=srgb&dl=pexels-pixabay-255329.jpg&fm=jpg",
    title: "A l'assaud du pôle Nord, le toit du monde",
    destination: "Pôle Nord: Cercle polaire, Russie, Spitzberg",
    description:
      "Atteindre le toit du monde à bord du brise-glace le plus puissant du monde...",
    longDescription:
      "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...Nous sommes engagés dans une démarche de tourisme responsable.Nous souhaitons vous faire connaître et vous associer à notre action aux côtés d'Agir pour un Tourisme Responsable (ATR). Pour plus d’informations avant de partir, lisez la charte éthique du voyageur et notre charte GNGL.En route vers une belle planète...Un trajet collectif Et n'oubliez pas qu'un geste pour la planète commence dès le début du voyage: pensez au covoiturage pour vos pré- acheminements! Une façon conviviale de diminuer votre consommation de CO2 et votre budget transport",
    avantages: "Circuit accompagné, Croisière et voile, 10 jours, vol inclus",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    tags: "",
    like: 90,
    duree: 24,
    price: 29799,
  });
  travels.push({
    id: 6,
    image:
      "https://images.pexels.com/photos/326916/pexels-photo-326916.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "Découverte de la côte sud de l'Islande",
    destination: "Pôle Nord: Islande",
    description:
      "Explorez les merveilles de la côte sud: chutes d'eau glacées, plages de sable noire, volcans et glaciers",
    longDescription:
      "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...Nous sommes engagés dans une démarche de tourisme responsable.Nous souhaitons vous faire connaître et vous associer à notre action aux côtés d'Agir pour un Tourisme Responsable (ATR). Pour plus d’informations avant de partir, lisez la charte éthique du voyageur et notre charte GNGL.En route vers une belle planète...Un trajet collectif Et n'oubliez pas qu'un geste pour la planète commence dès le début du voyage: pensez au covoiturage pour vos pré- acheminements! Une façon conviviale de diminuer votre consommation de CO2 et votre budget transport",
    avantages:
      "Circuit accompagné, Voyage sur mesure, Randonnée, 8 jours, vol inclus",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    tags: "",
    like: 90,
    duree: 24,
    price: 2970,
  });
  travels.push({
    id: 7,
    image:
      "https://images.pexels.com/photos/1009136/pexels-photo-1009136.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "Découverte des aurore boréales d'Alaska",
    destination: "Pôle Nord: Alaska",
    description: "Aventure hors normes. Explorez les merveilles de la côte...",
    longDescription:
      "L’hiver au Spitzberg offre de multiples facettes, de la nuit complète en décembre au jour permanent fin avril. Le retour du soleil  illumine les paysages et en révèle la beauté. Sorties en motoneige et en  traîneau, visite des entrailles du glacier et du musée retraçant l'histoire  seront au programme.",
    avantages:
      "Circuit accompagné, Voyage sur mesure, Randonnée, 12 jours, vol inclus",
    tags: "",
    like: 90,
    duree: 24,
    price: 12070,
  });
  travels.push({
    id: 8,
    image:
      "https://images.pexels.com/photos/3670415/pexels-photo-3670415.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    title: "Du Yukon à l'Alaska",
    destination: "Pôle Nord: Alaska, Canada",
    description:
      "Alaska et Yukon, deux noms magiques qui évoquent des territoires sauvages....",
    longDescription:
      "Alaska et Yukon, deux noms magiques qui évoquent des territoires sauvages, l'histoire des chercheurs d'or et des paysages hors normes. Vous traversez ces deux régions de Whitehorse à Anchorage, en parcourant des panoramas d'une beauté époustouflante : lacs miroirs, montagnes enneigées coiffées de glaciers, forêts à perte de vue et un littoral exceptionnel. Vous êtes ici sur une terre à la nature intacte où la faune est omniprésente : grizzlis, caribous, orignaux, baleines... Ce bout du monde est riche en contrastes, et c'est à travers un périple en minibus ponctué de randonnées que nous vous proposons de découvrir ces provinces mythiques d'Amérique du Nord.",
    detailDescription: "Circuit accompagné, À partir de 7 ans, 16 Jours",
    tags:
      "Faune, Flore, Pacifique, Expédition, Amérique, Creek, Alaska, Anchorage, McKinley, Denali, Yukon, Klondike, Kluane, Eldorado.",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    avantages:
      "• L'observation de la faune : ours, caribous, mouflons de Dall • Les randonnées variées et accessibles • La diversité des paysages: chaînes de montagnes et glaciers • L'histoire de la ruée vers l'o",
    like: 90,
    duree: 24,
    price: 6655,
  });
  travels.push({
    id: 9,
    image:
      "https://images.pexels.com/photos/464345/pexels-photo-464345.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    title: "Eclipse solaire et grand tour Antarctique",
    destination: "Pôle Sud: Antartique",
    description: "Vivre une éclipse solaire depuis l’Antarctique,... ",
    longDescription:
      "Vivre une éclipse solaire est un événement en soi du fait de la rareté du phénomène. Mais quand l’observation se fait depuis l’Antarctique, le caractère exceptionnel de l’événement en est décuplé et l’on atteint alors les superlatifs. Cette croisière permet une découverte très complète des régions antarctiques et subantarctiques et leur faune majestueuse.L’itinéraire prévu permettra ainsi de découvrir les Malouines, la Géorgie du Sud, les îles Shetland du Sud et la péninsule Antarctique.",
    activity: "Observation animalière, Astronomie, Découverte",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    tags:
      "Faune, Baleine, Fjord, Iceberg, Ushuaia, Bateaux d'expédition, Shetland du Sud, Shetland, Ernest Shackleton, Orcades.",
    like: 90,
    duree: 24,
    avantages: "• L'observation de l'éclipse solaire,",
    price: 19376,
  });
  travels.push({
    id: 10,
    image:
      "https://images.pexels.com/photos/6564672/pexels-photo-6564672.png?auto=compress&cs=tinysrgb&dpr=1&w=500",
    title: "Odyssée antarctique",
    destination: "Pôle Sud;  Argentine, Antartique, Nouvelle-Zélande",
    description:
      "Tour de l'Antarctique entre la Terre de Feu et la Nouvelle-Zélande...",
    longDescription:
      "Durant cette croisière en Antarctique, le bateau d’expédition Ortelius navigue sur les traces glorieuses des explorateurs polaires de la fin du XIXe et du début du XXe siècle. Jean-Baptiste Charcot, Roald Amundsen, Robert Falcon Scott ou Ernest Shackleton… tous éprouvèrent pour cette terre inconnue une attirance teintée de crainte, le besoin irrépressible d’y revenir, d’aller plus loin. Notre objectif : un tour de l'Antarctique entre la Terre de Feu et la Nouvelle-Zélande, avec au programme la plate-forme glaciaire de Ross, vaste comme la France ; l’île Pierre Ier, aussi flamboyante et terrifiante que le tsar à qui elle doit son nom ; l’île Macquarie, patrimoine mondial de l’Unesco, sculptée par des vents violents et des tempêtes de mer. Les manchots, les oiseaux des hautes latitudes jalonneront cette odyssée hors du temps. Du blanc, tous les dégradés de bleu formeront la toile de fond de ce paysage hors pair, offrant toutes les nuances et toutes les formes de glace depuis les énormes icebergs tabulaires aux sculptures de glace aux formes les plus futuristes.",
    tags:
      "Faune, Baleine, Iceberg, Expédition, Phoque, Ushuaia, Banquise, Bateaux d'expédition, Drake, Manchot empereur, Ile de Ross.",
    like: 80,
    activity: "Observation animalière, Astronomie, Découverte",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    avantages:
      "• La découverte de la plate-forme de glace de Ross. • Un navire spécialement renforcé glace. • Les survols et déposes en hélicoptèr.• La visite de bases scientifiques et sites historiques. • Les différentes espèces de manchots",
    duree: 33,
    price: 29310,
  });
  travels.push({
    id: 11,
    image:
      "https://images.pexels.com/photos/6969512/pexels-photo-6969512.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    title: "Vue sur les glaciers du Groenland",
    destination: "Pôle Nord: Groenland",
    description:
      "Bienvenue dans le dédale impressionnant des fjords les plus profonds...",
    longDescription:
      "Cette région du Groenland offre aux randonneurs le choc d’une mercouverte d’icebergs et l’impressionnante beauté des fronts glaciaires tombant dans la mer.voyage à la portée de tout randonneur. Les transferts en bateau dans les fjords entre les icebergs bleus vous permettent d’accéder à des randonnées à la journée, en faisant une  marche sur glacier avec ses grandes fissures et crevasses et en visitant d’anciens sites  vikings. Bienvenue dans le dédale impressionnant des fjords les plus profonds et des couleurs bleutées de la glace du Groenland ",
    tags:
      "Faune, Baleine, Iceberg, Expédition, Phoque, Tasermiut, Glaciers - Qaleraliq - GroenlandBanquise, Bateaux d'expédition, Drake, Manchot empereur, Ile de Ross.",
    like: 70,
    activity: "Randonnée, Trecking, Astronomie",
    physicalConditions:
      "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
    avantages: "Randonnée - Trecking - Astronomie -Vue sur les glaciers",
    duree: 17,
    price: 7860,
  });

  store.set("travels", travels);
}

let mainWindow = null;
let detailsTravelWindow = travels;
/**
 * Create HOME window function
 * @param {*} viewPath
 * @param {*} width
 * @param {*} height
 */
function createWindow(viewPath, width = 2000, height = 1400) {
  const win = new BrowserWindow({
    width: width,
    height: height,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      enableRemoteModule: false,
      preload: path.join(__dirname, "preload.js"),
    },
  });

  win.loadFile(viewPath); // loading viewPath

  return win;
}

//////////////////// STATIC CODE ////////////////////////
// when it's READY, it's create a window, here => (mainWindow)
app.whenReady().then(() => {
  mainWindow = createWindow(path.join(__dirname, "views/home/home.html"));

  mainWindow.webContents.on("did-finish-load", () => {
    mainWindow.send("init-data", { travels }); // use shot syntax from ES6 to travels
  });
});

app.on("window-all-closed", () => {
  // darwin === MacOS support
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    mainWindow = createWindow(path.join(__dirname, "views/home/home.html"));

    mainWindow.webContents.on("did-finish-load", () => {
      mainWindow.send("init-data", { travels }); // syntax from ES6, // when it's load, it send travels data to the view on channel "init-data"
    });
  }
});

///////////////////// IPC ON/HANDLER /////////////////////
/////////////////// add travel //////////////////////////

ipcMain.on("open-add-travel-window", (e, data) => {
  let addTravelWindow = createWindow("views/add-item/add-item.html", 1000, 700);
  ipcMain.handle("new-travel", (e, newTravel) => {
    newTravel.id = 1;

    if (travels.length > 0) {
      newTravel.id = travels[travels.length - 1].id + 1;
    }

    travels.push(newTravel); // push new object in travels array

    // STORAGE: Persist the new array on the JSON file
    store.set("travels", travels); // set: key, value,

    mainWindow.webContents.send("new-travel", newTravel);

    return "Le nouveau voyage a correctement été ajouté!";
  });

  addTravelWindow.on("closed", () => {
    ipcMain.removeHandler("new-travel");
  });
});

////////////// CREATE A NEW DETAIL CARD TRAVEL WINDOW /////////////
ipcMain.on("open-travel-details-window", (e, travelDetails) => {
  //travelDetails = data
  let detailsTravelWindow = createWindow(
    "views/details-travel/details-travel.html",
    1700,
    1000
  );

  detailsTravelWindow.webContents.on("did-finish-load", () => {
    detailsTravelWindow.send("init-travel-data", travelDetails);
  });

  ipcMain.handle("details-travel", (e, travelDetails) => {
    for (let [index, travel] of travels.entries()) {
      if (travel.id === editedTravel.id) {
        travels[index] = editedTravel;
        break;
      }
    }

    store.set("travels", travels);

    mainWindow.webContents.send("init-travel-data", travelDetails);
    detailsTravelWindow.webContents.send("init-travel-data", travelDetails);

    return "";
  });

  detailsTravelWindow.on("closed", () => {
    ipcMain.removeHandler("details-travel");
  });
});
///////////// EDIT TRAVEL ///////////////

ipcMain.on("open-edit-travel-window", (e, editedTravel) => {
  // create a new window
  let editTravelWindow = createWindow(
    "views/edit-item/edit-item.html",
    1000,
    500
  );

  editTravelWindow.webContents.on("did-finish-load", () => {
    editTravelWindow.send("init-data", editedTravel);
  });

  ipcMain.handle("edit-travel", (e, editedTravel) => {
    for (let [index, travel] of travels.entries()) {
      if (travel.id === editedTravel.id) {
        travels[index] = editedTravel;
        break;
      }
    }

    store.set("travels", travels);

    mainWindow.reload();

    return "Le voyage a correctement été modifié !";
  });

  editTravelWindow.on("closed", () => {
    ipcMain.removeHandler("edit-travel");
  });
});
// ipcMain.on("open-edit-travel-window", (e, travelToEdit) => {
//   const win = createWindow(
//     path.join(__dirname, "views/edit-item/edit-item.html", 1000, 500)
//   );

//   win.webContents.on("did-finish-load", () => {
//     win.send("init-data", travelToEdit);
//   });

//   ipcMain.handle("edit-travel", (e, editedTravel) => {
//     for (let [index, travel] of travels.entries()) {
//       if (travel.id === editedTravel.id) {
//         travels[index] = editedTravel;
//         break;
//       }
//     }

//     // Persist the edited array on the JSON file
//     store.set("travels", travels);

//     mainWindow.reload();

//     //mainWindow.webContents.send("travel-edited", editedTravel);
//     return "Le voyage a correctement été modifié !";
//   });
//   win.on("closed", () => {
//     ipcMain.removeHandler("edit-item");
//   });
// });

// ipcMain.on("open-travel-details-window", (e, travelDetails) => {
//   //travelDetails = data
//   const win = createWindow(
//     path.join(__dirname, "views/details-travel/details-travel.html"),
//     1400,
//     1000
//   );

//   win.webContents.on("did-finish-load", () => {
//     win.send("init-data", travelDetails);
//   });
// });

// ipcMain.handle("details-travel", (e, travelDetails) => {
//   for (let travel of travels) {
//     if (travel.id === travelDetails.id) {
//       travel = travelDetails;
//       break;
//     }
//   }

//   store.set("travels", travels);

//   mainWindow.webContents.send("init-data", travelDetails);

//   return { travels };
// });
// win.on("closed", () => {
//   ipcMain.removeHandler("init-data");
// });

///////////// DELETE TRAVEL WINDOW //////////////////////////

ipcMain.handle("show-confirm-delete-travel", (e, travelId) => {
  const choice = dialog.showMessageBoxSync({
    type: "warning",
    buttons: ["Non", "Oui"],
    title: "Confirmation de suppression",
    message: "Êtes-vous sûr de vouloir supprimer ce voyage?",
  });

  if (choice) {
    for (let [index, travel] of travels.entries()) {
      if (travel.id === travelId) {
        travels.splice(index, 1);
        break;
      }
    }
    store.set("travels", travels);
    mainWindow.reload();
  }

  return choice;
});

// const { app, BrowserWindow, ipcMain, dialog } = require("electron");
// const path = require("path");
// const Store = require("electron-store");

// const store = new Store();

// const travels = store.has("travels") ? store.get("travels") : [];

// if (travels.length === 0) {
//   travels.push({
//     id: 1,
//     image:
//       "https://images.pexels.com/photos/2356087/pexels-photo-2356087.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//     title: "Randonnée entre toundra, glaciers et icebergs",
//     destination: "Pôle Nord: Groenland",
//     description:
//       "Cette région du Groenland offre aux randonneurs le choc d’une mer couverte d’icebergs...",
//     longDescription:
//       "Cette région du Groenland offre aux randonneurs le choc d’une mer couverte d’icebergs et l’impressionnante beauté des fronts glaciaires tombant dans la mer. Le contraste des couleurs entre les glaces et la toundra luxuriante donne du caractère à ce voyage à la portée de tout randonneur. Les transferts en bateau dans les fjords entre les icebergs bleus vous permettent d’accéder à des randonnées à la journée, en faisant une marche sur glacier avec ses grandes fissures et crevasses et en visitant d’anciens sites vikings. Bienvenue dans le dédale impressionnant des fjords les plus profonds et des couleurs bleutées de la glace du Groenland ! Ces tarifs sont valables pour des départs de Paris. Des départs d'autres villes sont possibles, n'hésitez pas à nous en faire la demande. Les prix indiqués ne sont valables que pour certaines classes de réservation sur les compagnies aériennes qui desservent cette destination. Ils sont donc susceptibles d'être modifiés en cas d'indisponibilité de places dans ces classes au moment de la réservation. Le prix comprend Le transport aérien et les transferts, 2 nuits en Islande en guest house, 4 nuits au Groenland en auberge, 3 nuits au Groenland sous tente montée, le guide anglophone ou francophone suivant les dates, la pension complète au Groenland du dîner du J2 au petit déjeuner du J9, une parka chaude et un gilet de sauvetage pour les transfères en bateaux, les crampons et équipement de sécurité. Le prix ne comprend pas: Les frais d'inscription, les assurances, les repas en Islande et au Groenland non mentionnés, les excursions en option et entrées dans les musées.Complément d'information tarifaire: Si vous voyagez seul(e), un supplément single de 180 € sera appliqué sur le prix de votre voyage. A noter qu'il n'y a pas de possibilité de chambre single au Groenland. Ce supplément ne s’applique que pour les nuits en Islande",
//     tags: "Fjord, Iceberg, Arctique, Reykjavik, Banquise, Inlandsis",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     avantages: "Circuit accompagné, Randonnée, 10 jours, vol inclus",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 4999,
//   });
//   travels.push({
//     id: 2,
//     image:
//       "https://images.unsplash.com/photo-1615376506233-f0821173ddee?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDZ8fHxlbnwwfHx8&auto=format&fit=crop&w=500&q=60",
//     title: "Fjords et glaciers en kayak au Spitzberg",
//     destination: "Pôle Nord: Spitzberg",
//     description:
//       "Perchés tout au nord de l'Immense Isfjord, se trouvent les fjords Dickson et Ekman...",
//     longDescription:
//       "Circuit accompagné, Croisière, 9 jours, vol inclus: Nous vous proposons une découverte très confidentielle des territoires les plus inexplorés de l'Isfjord. Perchés tout au nord de l'Immense Isfjord, se trouvent les fjords Dickson et Ekman, deux fjords cachés aux yeux de tous et qui renferment plusieurs trésors. Tout d'abord, les montagnes du Kapitol et celles du Kolosseum, véritables sculptures minérales orchestrées par l'érosion naturelle. S'ensuit l'imposant glacier Sefstrombreen, avec un front de glace de près de 2,9 km de long. Notre campement sur l'île de Flintholem nous donne une vue extraordinaire sur ces premiers trésors. Nous terminons notre exploration par l'immense glacier de Svéa et ses 3,6 km de large. Un monstre de glace d’où résonne l'étrange grondement de la glace en mouvement. Un voyage extraordinaire, loin, très loin des sentiers battus.",
//     avantages: "Circuit accompagné, Croisière, 9 jours, vol inclus",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 3699,
//   });
//   travels.push({
//     id: 3,
//     image:
//       "https://images.pexels.com/photos/219837/pexels-photo-219837.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//     title: "Cap Horn et détroit de Magellan",
//     destination: "Pôle Sud: Chili, Argentine",
//     description:
//       "Les deux bateaux, le Stella Australis et le Ventus Australis, sont 2 navires similaires de la flotte chilienne. Uniques en leur genre",
//     longDescription:
//       "Les deux bateaux, le Stella Australis et le Ventus Australis, sont 2 navires similaires de la flotte chilienne. Uniques en leur genre, ils proposent des navigations dans le détroit de Magellan pour aller à la découverte des glaciers de la cordillère Darwin, en Terre de Feu. Après avoir emprunté le canal Beagle, le bateau descend jusqu'au cap Horn, avec un possible débarquement sur l'île Horn. Vous pourrez saluer les gardiens du phare le plus mythique. Ensuite, le bateau remonte vers la ville la plus australe du monde, Ushuaia. Il est possible d'effectuer la navigation dans le sens inverse (les deux navigations proposées sont complémentaires).",
//     avantages:
//       "• Le mythique cap Horn et le canal Beagle, • La découverte des glaciers de la cordillère Darwin, • Des bateaux de petite capacité, • Des débarquements quotidiens, • Une prestation tout inclus à bord",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 3999,
//   });
//   travels.push({
//     id: 4,
//     image:
//       "https://images.unsplash.com/photo-1614597388646-fea96377eb91?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
//     title: "Découvert du désert de Sibérie",
//     destination: "Pôle Nord: Sibérie",
//     description:
//       "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence...",
//     longDescription:
//       "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...Nous sommes engagés dans une démarche de tourisme responsable.Nous souhaitons vous faire connaître et vous associer à notre action aux côtés d'Agir pour un Tourisme Responsable (ATR). Pour plus d’informations avant de partir, lisez la charte éthique du voyageur et notre charte GNGL.En route vers une belle planète...Un trajet collectif Et n'oubliez pas qu'un geste pour la planète commence dès le début du voyage: pensez au covoiturage pour vos pré- acheminements! Une façon conviviale de diminuer votre consommation de CO2 et votre budget transport",
//     avantages: "Circuit accompagné, Randonnée, 12 jours, vol inclus",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 5499,
//   });
//   travels.push({
//     id: 5,
//     image:
//       "https://images.pexels.com/photos/255329/pexels-photo-255329.jpeg?cs=srgb&dl=pexels-pixabay-255329.jpg&fm=jpg",
//     title: "A l'assaud du pôle Nord, le toit du monde",
//     destination: "Pôle Nord: Cercle polaire, Russie, Spitzberg",
//     description:
//       "Atteindre le toit du monde à bord du brise-glace le plus puissant du monde...",
//     longDescription:
//       "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...Nous sommes engagés dans une démarche de tourisme responsable.Nous souhaitons vous faire connaître et vous associer à notre action aux côtés d'Agir pour un Tourisme Responsable (ATR). Pour plus d’informations avant de partir, lisez la charte éthique du voyageur et notre charte GNGL.En route vers une belle planète...Un trajet collectif Et n'oubliez pas qu'un geste pour la planète commence dès le début du voyage: pensez au covoiturage pour vos pré- acheminements! Une façon conviviale de diminuer votre consommation de CO2 et votre budget transport",
//     avantages: "Circuit accompagné, Croisière et voile, 10 jours, vol inclus",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 29799,
//   });
//   travels.push({
//     id: 6,
//     image:
//       "https://images.pexels.com/photos/326916/pexels-photo-326916.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//     title: "Découverte de la côte sud de l'Islande",
//     destination: "Pôle Nord: Islande",
//     description:
//       "Explorez les merveilles de la côte sud: chutes d'eau glacées, plages de sable noire, volcans et glaciers",
//     longDescription:
//       "L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...L’équilibre écologique des régions que vous allez traverser est très précaire.La permanence des flux touristiques, même en groupes restreints, le perturbe d’autant plus rapidement.Dans l’intérêt de tous, chaque participant est responsable de la propreté et de l’état des lieux qu’il traverse et où il campe le cas échéant.La lutte contre la pollution doit être l’affaire de chacun.Même si vous constatez que certains sites sont déjà pollués, vous devez ramasser tous vos papiers, mouchoirs en papier, boîtes, etc...Nous sommes engagés dans une démarche de tourisme responsable.Nous souhaitons vous faire connaître et vous associer à notre action aux côtés d'Agir pour un Tourisme Responsable (ATR). Pour plus d’informations avant de partir, lisez la charte éthique du voyageur et notre charte GNGL.En route vers une belle planète...Un trajet collectif Et n'oubliez pas qu'un geste pour la planète commence dès le début du voyage: pensez au covoiturage pour vos pré- acheminements! Une façon conviviale de diminuer votre consommation de CO2 et votre budget transport",
//     avantages:
//       "Circuit accompagné, Voyage sur mesure, Randonnée, 8 jours, vol inclus",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 2970,
//   });
//   travels.push({
//     id: 7,
//     image:
//       "https://images.pexels.com/photos/1009136/pexels-photo-1009136.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//     title: "Découverte des aurore boréales d'Alaska",
//     destination: "Pôle Nord: Alaska",
//     description: "Aventure hors normes. Explorez les merveilles de la côte...",
//     longDescription:
//       "L’hiver au Spitzberg offre de multiples facettes, de la nuit complète en décembre au jour permanent fin avril. Le retour du soleil  illumine les paysages et en révèle la beauté. Sorties en motoneige et en  traîneau, visite des entrailles du glacier et du musée retraçant l'histoire  seront au programme.",
//     avantages:
//       "Circuit accompagné, Voyage sur mesure, Randonnée, 12 jours, vol inclus",
//     tags: "",
//     like: 90,
//     duree: 24,
//     price: 12070,
//   });
//   travels.push({
//     id: 8,
//     image:
//       "https://images.pexels.com/photos/3670415/pexels-photo-3670415.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//     title: "Du Yukon à l'Alaska",
//     destination: "Pôle Nord: Alaska, Canada",
//     description:
//       "Alaska et Yukon, deux noms magiques qui évoquent des territoires sauvages....",
//     longDescription:
//       "Alaska et Yukon, deux noms magiques qui évoquent des territoires sauvages, l'histoire des chercheurs d'or et des paysages hors normes. Vous traversez ces deux régions de Whitehorse à Anchorage, en parcourant des panoramas d'une beauté époustouflante : lacs miroirs, montagnes enneigées coiffées de glaciers, forêts à perte de vue et un littoral exceptionnel. Vous êtes ici sur une terre à la nature intacte où la faune est omniprésente : grizzlis, caribous, orignaux, baleines... Ce bout du monde est riche en contrastes, et c'est à travers un périple en minibus ponctué de randonnées que nous vous proposons de découvrir ces provinces mythiques d'Amérique du Nord.",
//     detailDescription: "Circuit accompagné, À partir de 7 ans, 16 Jours",
//     tags:
//       "Faune, Flore, Pacifique, Expédition, Amérique, Creek, Alaska, Anchorage, McKinley, Denali, Yukon, Klondike, Kluane, Eldorado.",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     avantages:
//       "• L'observation de la faune : ours, caribous, mouflons de Dall • Les randonnées variées et accessibles • La diversité des paysages: chaînes de montagnes et glaciers • L'histoire de la ruée vers l'o",
//     like: 90,
//     duree: 24,
//     price: 6655,
//   });
//   travels.push({
//     id: 9,
//     image:
//       "https://images.pexels.com/photos/464345/pexels-photo-464345.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
//     title: "Eclipse solaire et grand tour Antarctique",
//     destination: "Pôle Sud: Antartique",
//     description: "Vivre une éclipse solaire depuis l’Antarctique,... ",
//     longDescription:
//       "Vivre une éclipse solaire est un événement en soi du fait de la rareté du phénomène. Mais quand l’observation se fait depuis l’Antarctique, le caractère exceptionnel de l’événement en est décuplé et l’on atteint alors les superlatifs. Cette croisière permet une découverte très complète des régions antarctiques et subantarctiques et leur faune majestueuse.L’itinéraire prévu permettra ainsi de découvrir les Malouines, la Géorgie du Sud, les îles Shetland du Sud et la péninsule Antarctique.",
//     activity: "Observation animalière, Astronomie, Découverte",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     tags:
//       "Faune, Baleine, Fjord, Iceberg, Ushuaia, Bateaux d'expédition, Shetland du Sud, Shetland, Ernest Shackleton, Orcades.",
//     like: 90,
//     duree: 24,
//     avantages: "• L'observation de l'éclipse solaire,",
//     price: 19376,
//   });
//   travels.push({
//     id: 10,
//     image:
//       "https://images.pexels.com/photos/6564672/pexels-photo-6564672.png?auto=compress&cs=tinysrgb&dpr=1&w=500",
//     title: "Odyssée antarctique",
//     destination: "Pôle Sud;  Argentine, Antartique, Nouvelle-Zélande",
//     description:
//       "Tour de l'Antarctique entre la Terre de Feu et la Nouvelle-Zélande...",
//     longDescription:
//       "Durant cette croisière en Antarctique, le bateau d’expédition Ortelius navigue sur les traces glorieuses des explorateurs polaires de la fin du XIXe et du début du XXe siècle. Jean-Baptiste Charcot, Roald Amundsen, Robert Falcon Scott ou Ernest Shackleton… tous éprouvèrent pour cette terre inconnue une attirance teintée de crainte, le besoin irrépressible d’y revenir, d’aller plus loin. Notre objectif : un tour de l'Antarctique entre la Terre de Feu et la Nouvelle-Zélande, avec au programme la plate-forme glaciaire de Ross, vaste comme la France ; l’île Pierre Ier, aussi flamboyante et terrifiante que le tsar à qui elle doit son nom ; l’île Macquarie, patrimoine mondial de l’Unesco, sculptée par des vents violents et des tempêtes de mer. Les manchots, les oiseaux des hautes latitudes jalonneront cette odyssée hors du temps. Du blanc, tous les dégradés de bleu formeront la toile de fond de ce paysage hors pair, offrant toutes les nuances et toutes les formes de glace depuis les énormes icebergs tabulaires aux sculptures de glace aux formes les plus futuristes.",
//     tags:
//       "Faune, Baleine, Iceberg, Expédition, Phoque, Ushuaia, Banquise, Bateaux d'expédition, Drake, Manchot empereur, Ile de Ross.",
//     like: 80,
//     activity: "Observation animalière, Astronomie, Découverte",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     avantages:
//       "• La découverte de la plate-forme de glace de Ross. • Un navire spécialement renforcé glace. • Les survols et déposes en hélicoptèr.• La visite de bases scientifiques et sites historiques. • Les différentes espèces de manchots",
//     duree: 33,
//     price: 29310,
//   });
//   travels.push({
//     id: 11,
//     image:
//       "https://images.pexels.com/photos/6969512/pexels-photo-6969512.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
//     title: "Vue sur les glaciers du Groenland",
//     destination: "Pôle Nord: Groenland",
//     description:
//       "Bienvenue dans le dédale impressionnant des fjords les plus profonds...",
//     longDescription:
//       "Cette région du Groenland offre aux randonneurs le choc d’une mercouverte d’icebergs et l’impressionnante beauté des fronts glaciaires tombant dans la mer.voyage à la portée de tout randonneur. Les transferts en bateau dans les fjords entre les icebergs bleus vous permettent d’accéder à des randonnées à la journée, en faisant une  marche sur glacier avec ses grandes fissures et crevasses et en visitant d’anciens sites  vikings. Bienvenue dans le dédale impressionnant des fjords les plus profonds et des couleurs bleutées de la glace du Groenland ",
//     tags:
//       "Faune, Baleine, Iceberg, Expédition, Phoque, Tasermiut, Glaciers - Qaleraliq - GroenlandBanquise, Bateaux d'expédition, Drake, Manchot empereur, Ile de Ross.",
//     like: 70,
//     activity: "Randonnée, Trecking, Astronomie",
//     physicalConditions:
//       "Le voyage est accessible à toute personne en bonne condition physique et aimant la vie en plein air, les nuits sous tente dans un sac de couchage, et habituée à marcher en montagne régulièrement. Le trekking le plus long comporte 6 à 7h de marche réelle (avec sac à dos léger) sur un terrain facile et un dénivelé maximum de 600 mètres. Nous formons généralement deux groupes : l’un poursuivant la route et l’autre s’arrêtant à une côte intermédiaire. Les autres trekkings sont plus doux, de 3 à 5 heures, et en alternance avec des jours sans marche. Les randonnées sont faites de façon volontaire, les participants pouvant rester au campement ou à l’auberge s’ils le souhaitent. L’excursion avec crampons sur le glacier ne nécessite aucune expérience préalable. Un guide de haute montagne professionnel nous accompagnera sur ce trajet avec une totale sécurité. Si des doutes subsistent sur votre capacité physique à réaliser ce voyage, n’hésitez pas à nous contacter pour en discuter. Les aurores boréales sont l’une des merveilles naturelles de notre planète, un spectacle de lumières et de soulèvement avec lequel les cieux des régions polaires nous charment durant les nuits claires et calmes. Il est plus fréquent de les observer en hiver. Néanmoins, le sud du Groenland est réputé pour être l’une des meilleures zones du monde pour profiter de ce véritable spectacle naturel en plein été. Il est possible de les observer dès la mi-août si le ciel est dégagé.",
//     avantages: "Randonnée - Trecking - Astronomie -Vue sur les glaciers",
//     duree: 17,
//     price: 7860,
//   });

//   store.set("travels", travels);
// }

// let mainWindow = null;

// /**
//  * Create HOME window function
//  * @param {*} viewPath
//  * @param {*} width
//  * @param {*} height
//  */
// function createWindow(viewPath, width = 1400, height = 1000) {
//   const win = new BrowserWindow({
//     width: width,
//     height: height,
//     webPreferences: {
//       nodeIntegration: false,
//       contextIsolation: true,
//       enableRemoteModule: false,
//       preload: path.join(__dirname, "preload.js"),
//     },
//   });

//   win.loadFile(path.join(_dirname, viewPath)); // loading viewPath

//   return win;
// }

// //////////////////// STATIC CODE ////////////////////////
// // when it's READY, it's create a window, here => (mainWindow)
// app.whenReady().then(() => {
//   mainWindow = createWindow("views/home/home.html");

//   mainWindow.webContents.on("did-finish-load", () => {
//     mainWindow.webContents.send("init-data", { travels }); // use shot syntax from ES6 to travels
//   });
// });

// app.on("window-all-closed", () => {
//   // darwin === MacOS support
//   if (process.platform !== "darwin") {
//     app.quit();
//   }
// });

// app.on("activate", () => {
//   if (BrowserWindow.getAllWindows().length === 0) {
//     mainWindow = createWindow("views/home/home.html");

//     mainWindow.webContents.on("did-finish-load", () => {
//       mainWindow.webContents.send("init-data", { travels }); // syntax from ES6, // when it's load, it send travels data to the view on channel "init-data"
//     });
//   }
// });

// ///////////////////// IPC ON/HANDLER /////////////////////
// /////////////////// add travel //////////////////////////
// //const onOpenAddTravelWindow = (e, data) => {
// ipcMain.on("open-add-travel-window", (e, data) => {
//   let addTravelWindow = createWindow("views/add-item/add-item.html", 1000, 500);
//   ipcMain.handle("new-travel", (e, newTravel) => {
//     newTravel.id = 1;

//     if (travels.length > 0) {
//       newTravel.id = travels[travels.length - 1].id + 1;
//     }

//     travels.push(newTravel); // push new object in travels array

//     // STORAGE: Persist the new array on the JSON file
//     store.set("travels", travels); // set: key, value,

//     mainWindow.webContents.send("new-travel-added", newTravel);

//     return "Le nouveau voyage a correctement été ajouté!";
//   });

//   addTravelWindow.on("closed", () => {
//     ipcMain.removeHandler("new-travel");
//   });
// });

// //ipcMain.on("open-add-travel-window", onOpenAddTravelWindow);

// ////////////// DETAIL TRAVEL /////////////
// ipcMain.on("open-travel-details-window", (e, travelDetails) => {
//   //travelDetails = data
//   let detailsTravelWindow = createWindow(
//     "views/details-travel/details-travel.html",
//     1400,
//     1000
//   );

//   detailsTravelWindow.webContents.on("did-finish-load", () => {
//     detailsTravelWindow.send("init-data", travelDetails);
//   });
// });

// ipcMain.handle("details-travel", (e, travelDetails) => {
//   for (let travel of travels) {
//     if (travel.id === travelDetails.id) {
//       travel = travelDetails;
//       break;
//     }
//   }

//   store.set("travels", travels);

//   //mainWindow.webContents.send("init-data", travelDetails);
//   detailsTravelWindow.webContents.send("init-data", travelDetails);

//   return "";
// });
// detailsTravelWindow.on("closed", () => {
//   ipcMain.removeHandler("init-data");
// });

// ////////////// EDIT TRAVEL ///////////////
// ipcMain.on("open-edit-travel-window", (e, travelToEdit) => {
//   const editTravelWindow = createWindow(
//     "views/edit-item/edit-item.html",
//     1000,
//     500
//   );

//   editTravelWindow.webContents.on("did-finish-load", () => {
//     editTravelWindow.send("init-data", travelToEdit);
//   });

//   ipcMain.handle("edit-travel", (e, editedTravel) => {
//     for (let [index, travel] of travels.entries()) {
//       if (travel.id === editedTravel.id) {
//         travels[index] = editedTravel;
//         break;
//       }
//     }

//     // Persist the edited array on the JSON file
//     store.set("travels", travels);

//     mainWindow.reload();

//     //mainWindow.webContents.send("travel-edited", editedTravel);
//     return "Le voyage a correctement été modifié !";
//   });
//   editTravelWindow.on("closed", () => {
//     ipcMain.removeHandler("edit-item");
//   });
// });

// // ipcMain.on("open-travel-details-window", (e, travelDetails) => {
// //   //travelDetails = data
// //   const win = createWindow(
// //     path.join(__dirname, "views/details-travel/details-travel.html"),
// //     1400,
// //     1000
// //   );

// //   win.webContents.on("did-finish-load", () => {
// //     win.send("init-data", travelDetails);
// //   });
// // });

// // ipcMain.handle("details-travel", (e, travelDetails) => {
// //   for (let travel of travels) {
// //     if (travel.id === travelDetails.id) {
// //       travel = travelDetails;
// //       break;
// //     }
// //   }

// //   store.set("travels", travels);

// //   mainWindow.webContents.send("init-data", travelDetails);

// //   return { travels };
// // });
// // win.on("closed", () => {
// //   ipcMain.removeHandler("init-data");
// // });

// ///////////// DELETE TRAVEL WINDOW //////////////////////////

// ipcMain.handle("show-confirm-delete-travel", (e, travelId) => {
//   const choice = dialog.showMessageBoxSync({
//     type: "warning",
//     buttons: ["Non", "Oui"],
//     title: "Confirmation de suppression",
//     message: "Êtes-vous sûr de vouloir supprimer ce voyage?",
//   });

//   if (choice) {
//     for (let [index, item] of travels.entries()) {
//       if (item.id === travelId) {
//         travels.splice(index, 1);
//         break;
//       }
//     }

//     store.set("travels", travels);

//     mainWindow.reload();
//   }

//   return choice, travels;
// });
