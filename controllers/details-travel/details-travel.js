const { ipcRenderer } = require("electron");

const detailsOneCard = (travel) => {
  // Create elements needed to build a card
  const div = document.createElement("div");
  const img = document.createElement("img");
  const h3 = document.createElement("h3");
  const destinationCreatedEl = document.createElement("h4");
  const descriptionCreatedEl = document.createElement("p");
  const longDescriptionCreatedEl = document.createElement("p");
  const avantagesCreatedEl = document.createElement("p");
  const priceCreatedEl = document.createElement("h4");
  const a = document.createElement("a");

  // Append newly created elements into the DOM
  const body = document.querySelector("body");
  body.append(div);
  h3.append(a);
  div.append(img); // ADD NEW dimentions
  div.append(h3);
  div.append(destinationCreatedEl);
  div.append(descriptionCreatedEl);
  div.append(longDescriptionCreatedEl);
  div.append(avantagesCreatedEl);
  div.append(priceCreatedEl);

  // Set content and attributes
  a.innerHTML = travel.title;
  destinationCreatedEl.innerText = travel.destination;
  longDescriptionCreatedEl.innerText = travel.longDescription;
  priceCreatedEl.innerText = `à partir de ${travel.price} €`;
  avantagesCreatedEl.innerText = `Les + de cette aventure: ${travel.avantages}`;

  img.setAttribute("srcset", travel.image);
  img.setAttribute("class", "img-fluid");
  img.setAttribute("class", "max-widht:100%");
  img.setAttribute("class", "height:100%");
  //img.style.width = "639px";
  //img.style.height = "425px";
  div.setAttribute("class", "card");

  //   ////////////// CREATE ACTIONS BTN ////////////////////////////
  const actions = document.createElement("p");

  ///// UPDATE /////////
  const editBtn = document.createElement("button");
  editBtn.classList.add("btn", "btn-outline-warning", "mx-2");
  editBtn.innerText = "Modifier";

  const onClickEditBtn = () => {
    console.log(editBtn); // test on click btn
    ipcRenderer.send("open-edit-travel-window", travel);

    ipcRenderer.once("travel-edited", (e, travelEdited) => {
      img.innerText = travelEdited.img;
      h3.innerText = travelEdited.h3;
      descriptionCreatedEl.innerText = travelEdited.descriptionCreatedEl;
      destinationCreatedEl.innerText = travelEdited.destinationCreatedEl;
      avantagesCreatedEl.innerText = travelEdited.avantagesCreatedEl;
      priceCreatedEl.innerText = travelEdited.priceCreatedEl;
      longDescriptionCreatedEl.innerText =
        travelEdited.longDescriptionCreatedEl;
    });
  };
  editBtn.addEventListener("click", onClickEditBtn);

  const deleteBtn = document.createElement("button");
  deleteBtn.classList.add("btn", "btn-outline-danger", "mx-2");
  deleteBtn.innerText = "Supprimer";

  const onClickDeleteBtn = () => {
    console.log("ok");
    ipcRenderer.invoke("show-confirm-delete-travel", travel.id).then((res) => {
      if (res) {
        div.remove();
      }
    });
  };

  deleteBtn.addEventListener("click", onClickDeleteBtn); // add an event listener on click
  actions.append(editBtn, deleteBtn);

  div.append(
    img,
    priceCreatedEl,
    longDescriptionCreatedEl,
    a,
    avantagesCreatedEl,
    descriptionCreatedEl,
    destinationCreatedEl,
    actions
  );
};

////////////////////// INIT DATA //////////////////////

ipcRenderer.on("init-travel-data", (e, data) => {
  detailsOneCard(data);

  console.log(data);
});
