const { ipcRenderer } = require("electron");

const generateCards = (travel) => {
  // Retrieve the div card element
  const divCard = document.querySelector("#travels-card");

  // Create elements needed to build a card
  const divGlobalCardEl = document.createElement("div"); // create global card div

  const divToAddRowEl = document.createElement("div"); // create a div to add an horizontal card

  const divImgCardEl = document.createElement("div"); // create a div for image content, with 4 column in the 12 of the grid
  const divImgTextOverlayEl = document.createElement("div"); // create a div to put text on the image with an overlay
  const imgEl = document.createElement("img");

  // create second card's column
  const divTextCardEl = document.createElement("div"); // create a div for image content, with 8 columns, the rest of columns grid

  const divHeaderCardEl = document.createElement("div");
  const divBodyTextCardEl = document.createElement("div"); // create body of second card's column
  const divFooterCardEl = document.createElement("div");

  const titleEl = document.createElement("h3");
  const destinationCreatedEl = document.createElement("h4");
  const descriptionCreatedEl = document.createElement("p");

  const avantagesCreatedEl = document.createElement("p");
  /********* IF TIME, CREATE A LIST OF LI TO AVANTAGES OBJECT LIKE THIS ******************/
  /*html: */
  /*
     <ul id="listPlus">
        <li>• Les randonnées en étoile et déplacements en bateau au milieu des icebergs</li>
        <li>• Le site exceptionnel du camp de base de Qaleraliq au pied des glaciers</li>
        <li>• Une excursion avec crampons sur la calotte polaire</li>
        <li>• L'auberge de Qassiarssuq et son environnement de glace</li>
        <li>• La visite des sites historiques vikings et inuit </li>
      </ul>
  */

  const priceCreatedEl = document.createElement("h4");

  // Append newly created elements into the DOM
  const body = document.querySelector("body");

  body.append(divGlobalCardEl);

  divGlobalCardEl.append(divToAddRowEl);

  divToAddRowEl.append(divImgCardEl, divTextCardEl);

  divImgCardEl.append(imgEl, divImgTextOverlayEl);

  divTextCardEl.append(divHeaderCardEl, divBodyTextCardEl, divFooterCardEl);

  divHeaderCardEl.append(titleEl);
  divFooterCardEl.append(priceCreatedEl);

  divBodyTextCardEl.append(
    destinationCreatedEl,
    descriptionCreatedEl,
    avantagesCreatedEl
  );

  // Set content and attributes
  divGlobalCardEl.setAttribute("id", "id-div-card"); // add a id
  divGlobalCardEl.setAttribute("class", "card"); // add a class with a card CSS attribute
  divGlobalCardEl.classList.add("mb-3", "max-width:540px"); // add somes CSS attributes to the card class

  divToAddRowEl.setAttribute("class", "row"); // set a class on the div with a row css attribute
  divToAddRowEl.classList.add("g-0", "text-left");
  // divToAddRowEl.classList.add("g-6", "text-left");

  divImgCardEl.setAttribute("class", "col-md-4"); // add a class and a CSS atribute on it (represent 4 columns on the grid, so it rested 8 columns empty now )
  divImgTextOverlayEl.setAttribute("class", "card-img-overlay"); // add a class and an card-img-overlay bootstrap attribute on this class

  divHeaderCardEl.setAttribute("class", "card-header"),
    divBodyTextCardEl.setAttribute("class", "card-body");
  divFooterCardEl.setAttribute("class", "card-footer"),
    divTextCardEl.setAttribute("class", "col-md-8"); // add a class and a CSS atribute on it (represent columns rested on the grid)
  // divTextCardEl.setAttribute("class", "text-center");
  descriptionCreatedEl.setAttribute("class", "col-9", "text-truncate");

  titleEl.innerHTML = travel.title;
  destinationCreatedEl.innerText = travel.destination;
  descriptionCreatedEl.innerText = travel.description;
  priceCreatedEl.innerText = `à partir de ${travel.price} €`;
  avantagesCreatedEl.innerText = `Les + de cette aventure: ${travel.avantages}`;

  imgEl.setAttribute("src", travel.image);
  imgEl.setAttribute("class", "rounded", "img-fluid", "img-thumbnail");
  imgEl.setAttribute("class", "max-widht:100%");
  imgEl.setAttribute("class", "height:auto");

  ////////////// CREATE ACTIONS BTN ////////////////////////////

  const actions = document.createElement("p");

  //////////////////////////////////////////////////////////////
  ////////////////// DETAILS TRAVEL ////////////////////////////
  //////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////
  //  IMPORTANT TO DO: THINK TO REFACTOR CODE THAT CREATE     //
  //    NEW WINDOWS FUNCTIONS TO ADD MORE GENERICITY          //
  //////////////////////////////////////////////////////////////

  const detailsBtn = document.createElement("button"); // create button
  detailsBtn.setAttribute("id", "idDetailsBtn"); // set an id to the button
  detailsBtn.classList.add("btn", "btn-dark", "mx-2", "shadow"); // add css attributes to this button
  detailsBtn.innerText = "Plus d'Infos"; // Put text into this button

  const onClickTravelDetails = () => {
    //console.log(detailsBtn); // test on click btn
    ipcRenderer.send("open-travel-details-window", travel);
  };
  detailsBtn.addEventListener("click", onClickTravelDetails);
  console.log("DETAILS"); // test on click btn

  actions.append(detailsBtn); // editBtn, deleteBtn

  divBodyTextCardEl.append(actions);
};

////////////////////// INIT DATA //////////////////////

ipcRenderer.on("init-data", (e, data) => {
  data.travels.forEach(generateCards);

  //console.log(data);
});

//////////////////////// NEW ITEM ADDED LISTENER ////////
ipcRenderer.on("new-item-added", (e, data) => {
  imgEl.innerText = travelEdited.imgEl;
  titleEl.innerText = travelEdited.titleEl;
  descriptionCreatedEl.innerText = travelEdited.descriptionCreatedEl;
  destinationCreatedEl.innerText = travelEdited.destinationCreatedEl;
  avantagesCreatedEl.innerText = travelEdited.avantagesCreatedEl;
  priceCreatedEl.innerText = travelEdited.priceCreatedEl;

  generateCards("travels-card", data.item);
});

// ///// ADD TRAVEL //////
const addBtn = document.querySelector("#add-travel");
const onClickAddTravelBtn = () => {
  console.log("ok");
  ipcRenderer.send("open-add-travel-window"); //, travel
};
addBtn.addEventListener("click", onClickAddTravelBtn);

ipcRenderer.on("travel-added", (e, newTravel) => {
  generateCards(newTravel);
});
