const { ipcRenderer } = require("electron");

const editTravelForm = document.querySelector("#edit-travel-form");
const editTravelSubmit = newTravelForm.querySelector("#edit-travel-submit");

const destinationInput = editTravelForm.querySelector("#destination-name");
const imageInput = editTravelForm.querySelector("#image-name");
const titleInput = editTravelForm.querySelector("#title-name");
const avantagesInput = editTravelForm.querySelector("#avantages-name");
const priceInput = editTravelForm.querySelector("#price-name");
const descriptionInput = editTravelForm.querySelector("#description-name");
const longDescriptionInput = editTravelForm.querySelector(
  "#longDescription-name"
);

////////////////// INIT DATA ////////////////////
ipcRenderer.on("init-travel-data", (e, travelToEdit) => {
  destinationInput.value = travelToEdit.destination;
  imageInput.value = travelToEdit.image;
  titleInput.value = travelToEdit.title;
  avantagesInput.value = travelToEdit.avantages;
  priceInput.value = travelToEdit.price;
  descriptionInput.value = travelToEdit.description;
});

const onInputCheckValue = () => {
  if (
    destinationInput.value !== "" &&
    imageInput.value !== "" &&
    titleInput.value !== "" &&
    avantagesInput.value !== "" &&
    priceInput.value !== "" &&
    descriptionInput.value !== "" &&
    longDescriptionInput.value !== ""
  ) {
    editTravelSubmit.hidden = false;
  } else {
    editTravelSubmit.hidden = true;
  }
};

destinationInput.addEventListener("input", onInputCheckValue);
imageInput.addEventListener("input", onInputCheckValue);
titleInput.addEventListener("input", onInputCheckValue);
avantagesInput.addEventListener("input", onInputCheckValue);
priceInput.addEventListener("input", onInputCheckValue);
descriptionInput.addEventListener("input", onInputCheckValue);
longDescriptionInput.addEventListener("input", onInputCheckValue);

const onSubmitEditTravel = (e) => {
  e.preventDefault();

  const editedTravel = {
    destination: destinationInput.value,
    image: imageInput.value,
    title: titleInput.value,
    avantages: avantagesInput.value,
    price: priceInput.value,
    description: descriptionInput.value,
    longDescription: longDescriptionInput.value,
  };

  ipcRenderer.invoke("edit-travel", editedTravel).then((msg) => {
    const msgDiv = document.querySelector("#response-message");
    msgDiv.innerText = msg;
    msgDiv.hidden = false;

    // setTimeout function : hide the message after 2 seconds
    setTimeout(() => {
      msgDiv.hidden = true;
    }, 2000);
  });
};

editTravelForm.addEventListener("submit", onSubmitEditTravel);
