const { ipcRenderer } = require("electron");

//////////////////// FORM NEW TRAVEL ////////////////////
const newTravelForm = document.querySelector("#new-travel-form");
const newTravelSubmit = newTravelForm.querySelector("#new-travel-submit");

const destinationInput = newTravelForm.querySelector("#destination-name");
const imageInput = newTravelForm.querySelector("#image-name");
const titleInput = newTravelForm.querySelector("#title-name");
const avantagesInput = newTravelForm.querySelector("#avantages-name");
const priceInput = newTravelForm.querySelector("#price-name");
const descriptionInput = newTravelForm.querySelector("#description-name");
const longDescriptionInput = newTravelForm.querySelector(
  "#longDescription-name"
);

const onInputCheckValue = () => {
  if (
    destinationInput.value !== "" &&
    imageInput.value !== "" &&
    titleInput.value !== "" &&
    avantagesInput.value !== "" &&
    priceInput.value !== "" &&
    descriptionInput.value !== "" &&
    longDescriptionInput.value !== ""
  ) {
    newTravelSubmit.hidden = false;
  } else {
    newTravelSubmit.hidden = true;
  }
};

destinationInput.addEventListener("input", onInputCheckValue);
imageInput.addEventListener("input", onInputCheckValue);
titleInput.addEventListener("input", onInputCheckValue);
avantagesInput.addEventListener("input", onInputCheckValue);
priceInput.addEventListener("input", onInputCheckValue);
descriptionInput.addEventListener("input", onInputCheckValue);
longDescriptionInput.addEventListener("input", onInputCheckValue);

const onSubmitNewTravel = (e) => {
  e.preventDefault();

  const newTravel = {
    destination: destinationInput.value,
    image: imageInput.value,
    title: titleInput.value,
    avantages: avantagesInput.value,
    price: priceInput.value,
    description: descriptionInput.value,
    longDescription: longDescriptionInput.value,
  };

  ipcRenderer.invoke("new-travel", newTravel).then((msg) => {
    const msgDiv = document.querySelector("#response-message");
    msgDiv.innerText = msg;
    msgDiv.hidden = false;

    // hide the message after 2sec
    setTimeout(() => {
      msgDiv.hidden = true;
    }, 1500);

    // Reset the form and hide the button
    newTravelForm.reset();
    newTravelSubmit.hidden = true;
  });
};

newTravelForm.addEventListener("submit", onSubmitNewTravel);
